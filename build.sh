#!/usr/bin/env bash

[ -e ./build ] && rm -r ./build
cp -r ./src/ ./build